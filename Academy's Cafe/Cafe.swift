//
//  Cafe.swift
//  Academy's Cafe
//
//  Created by Ricky Austin on 06/06/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

class Cafe {
var kasir = Kasir()
    
    func menu(){
        print("""
        Hi, we have 5 Food & Beverage options for you!
        ----------------------------------------------
        [F03] Gado-Gado
        [F02] Chicken Satay
        [F01] Nasi Padang
        [B02] Mineral Water
        [B01] Ice Tea
        [Q] Back To Main Menu
        """)
    }
    
    func makanan(pesanan:String){
        print("Urusan Dapur")
    }
    
    func pembayaran(){
        print("Urusan Kasir")
    }
}

struct Kasir {
    var pesanan = ""
    var daftarPesanan = [String]()
}

class Dapur : Cafe {
    
    override func makanan(pesanan : String) {
        if pesanan.caseInsensitiveCompare("f03") == .orderedSame{
            kasir.pesanan = "Gado-Gado"
        }
        else if pesanan.caseInsensitiveCompare("f02") == .orderedSame{
            kasir.pesanan = "Chicken Satay"
        }
        else if pesanan.caseInsensitiveCompare("f01") == .orderedSame{
            kasir.pesanan = "Nasi Padang"
        }
        else if pesanan.caseInsensitiveCompare("b02") == .orderedSame{
            kasir.pesanan = "Mineral Water"
        }
        else if pesanan.caseInsensitiveCompare("b01") == .orderedSame{
            kasir.pesanan = "Ice Tea"
        }
    }
}


